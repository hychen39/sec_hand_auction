
```mermaid
classDiagram
class Book {
    id: int
    seller: User
    sellingStatus: String
}

class Transaction {
    id: int
    book: Book
}

class User {
    id: int
}

class Post {
    id: int
    book: Book
}

Transaction --> Book
Book --> User
Post --> Book
```