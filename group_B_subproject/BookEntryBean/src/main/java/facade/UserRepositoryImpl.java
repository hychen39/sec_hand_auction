/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import entities.User;
import java.io.IOException;
import java.util.HashMap;
import javax.swing.text.View;
import repository.AbstractRepository;

/**
 *
 * @author yaosiyu
 */
public class UserRepositoryImpl extends AbstractRepository<User>{
    protected int studentID;

    public UserRepositoryImpl(){
        super(new HashMap<Integer,User>());
    }
    
    @Override
    public int add(User entity){
        studentID = Integer.parseInt(entity.getMail().substring(1, 9));
        this.repository.put(studentID, new User(entity));
        
        int currentIdx = lastIndex.intValue();
        lastIndex++;
        return currentIdx;
    }
}
