/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import entities.Book;
import java.util.HashMap;
import java.util.Map;
import repository.AbstractRepository;
import java.lang.String;
import java.lang.Integer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;

/**
 *
 * @author yaosiyu
 */
@Singleton
public class BookRepositoryImpl extends AbstractRepository<Book> {

    private Logger logger;

    public BookRepositoryImpl() {
        super(new HashMap<Integer, Book>());
    }

    /*@Override
    public int add(Book entity){
        studentID = Integer.parseInt(entity.getSellerID());
        return 0;
    }*/
    /**
     * 查詢某個學生刊登銷售的書
     */
    public List<Book> findPersonalBook(String studentID) {
        List personalBook = new ArrayList();
        for (Map.Entry<Integer, Book> b : repository.entrySet()) {
            if (b.getValue().getSellerID().equals(studentID)) {
                personalBook.add(b.getValue());
            }
        }
        return personalBook;
    }
    
    @PostConstruct
    public void init() {
        // set the logger
        logger = Logger.getLogger(BookRepositoryImpl.class.getName());
        this.create_mockup_data();
        logger.info(String.format("Book Repository Size: %s ", this.size()));
    }

    /**
     * 建立書本測試資料
     *
     */
    private void create_mockup_data() {
        /*Book book = new Book();
        book.setBookName("微積分");
        book.setSellPrice(150);
        book.setCollege("資訊學院");
        book.setMajor("資管系");
        book.setTeacherName("李朱慧");
        book.setIsbn("9789866637605");
        book.setStatus("B");
        book.setSellerID("s10814111"); //s10814111
        book.setSituation("selling");
        book.setId(Long.valueOf(this.add(book)));
      
        logger.info(String.format("Create modkup data: %s", book.toString()));
        // Note: 測試賣家為 s10814111
        book = new Book();
        book.setBookName("Java 程式設計");
        book.setSellPrice(200);
        book.setCollege("資訊學院");
        book.setMajor("資管系");
        book.setTeacherName("曾顯文");
        book.setIsbn("1234567890123");
        book.setStatus("B");
        book.setSellerID("s10814111");
        book.setSituation("selling");
        book.setId(Long.valueOf(this.add(book)));
        logger.info(String.format("Create modkup data: %s", book.toString()));
        */
        
    }

    public int size() {
        return this.repository.size();
    }
}
