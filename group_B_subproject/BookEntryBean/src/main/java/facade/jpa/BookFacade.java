/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package facade.jpa;

import entities.Book;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author hychen39@gm.cyut.edu.tw
 * @since Sep 28, 2020
 */
@Stateless
public class BookFacade extends AbstractFacade<Book> {

    @PersistenceContext(unitName = "UsedBookAuction-PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BookFacade() {
        super(Book.class);
    }
    
    public List<Book> findBooksBySellerID(String sellerID){
        TypedQuery<Book> query;
        List<Book> resutls;
        query = getEntityManager().createNamedQuery("findBookBySellerID", Book.class)
                .setParameter("sellerID", sellerID);
        resutls = query.getResultList();
        return resutls;
    }
    
    /**
     * 使用者按下首頁搜尋鍵,用書名搜尋
     * @param bookName
     * @return 
     */
    public List<Book> findBooksByBookName(String bookName){
        TypedQuery<Book> query;
        List<Book> results;
        query = getEntityManager().createNamedQuery("findBookByBookName", Book.class)
                .setParameter("bookName", bookName);
        results = query.getResultList();
        return results;
    }
    /**
     * 使用者按下首頁搜尋鍵,用isbn搜尋
     * @param bookName
     * @return 
     */
    public List<Book> findBooksByBookIsbn(String isbn){
        TypedQuery<Book> query;
        List<Book> results;
        query = getEntityManager().createNamedQuery("findBookByBookIsbn", Book.class)
                .setParameter("isbn", isbn);
        results = query.getResultList();
        return results;
    }

}
