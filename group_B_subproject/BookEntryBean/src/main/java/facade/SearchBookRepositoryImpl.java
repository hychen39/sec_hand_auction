/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import entities.SearchBook;
import java.util.HashMap;
import javax.ejb.Singleton;
import repository.AbstractRepository;

/**
 *
 * @author yaosiyu
 */
@Singleton
public class SearchBookRepositoryImpl extends AbstractRepository<SearchBook>{
    
    public SearchBookRepositoryImpl(){
        super(new HashMap<Integer,SearchBook>());
    }
}
