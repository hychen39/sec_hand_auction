/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.logging.Logger;
import javax.ejb.EJB;
import usermgt.UserManagementBean;

/**
 * User Login and Logout
 *
 * @author hychen39@gm.cyut.edu.tw
 */
@Named(value = "loginBean")
@SessionScoped
public class LoginBean implements Serializable {

    @EJB
    private UserManagementBean userManagementBean;

    /**
     * Creates a new instance of LoginBean
     */
    public LoginBean() {
    }
    
    public boolean isLogin(){
        return userManagementBean.isLoginStatus();
    }
    /**
     * 
     * @return 
     */
    public String getUsername(){
        Logger.getLogger(LoginBean.class.getName()).info(String.format("Login Username: %s",
                userManagementBean.getPrincipalName()));
        return userManagementBean.getPrincipalName();
    }
}
