/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import entities.Book;
import facade.BookRepositoryImpl;
import facade.jpa.BookFacade;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.PrimeFaces;

/**
 *
 * @author yaosiyu
 */
@Named(value = "indexBean")
@ViewScoped
@Getter
@Setter
public class IndexBean implements Serializable {
    
    FacesContext facesContext;
    
    @EJB
    BookRepositoryImpl bookRepository;
    
    @EJB
    BookFacade bookFacade;
    
    private int selectedBookId;
    private List<Book> sellingBooks;
    private String searchType; //首頁搜尋類別：isbn, 書名
    private String userSearchKeyBook; //使用者搜尋的關鍵字
    

    /*public BookRepositoryImpl getBookRepository() {
        return bookRepository;
    }
    
    
    public void setBookRepository(BookRepositoryImpl bookRepository) {
        this.bookRepository = bookRepository;
    }

    public int getSelectedBookId() {
        return selectedBookId;
    }

    public void setSelectedBookId(int selectedBookId) {
        this.selectedBookId = selectedBookId;
    }
    */

    /**
     * Creates a new instance of IndexBean
     */
    public IndexBean() {
    }

    /**
     * 首頁第一次載入時,回傳所有銷售中的書
     * @return 
     */
    public List<Book> findAllSellingBook() {
        //return allSellingBook
        sellingBooks = bookFacade.findAll().stream()
                .filter(aBook -> aBook.getSituation().equals("selling"))
                .collect(Collectors.toList());
        return sellingBooks;
    }
    
    /**
     * 透過使用者選擇的搜尋類別和關鍵字,經過bookFacade到資料庫找書
     */
    public void findUserSearchKeyBooks(){
        
        switch(searchType){
            case "bookName":
                sellingBooks = bookFacade.findBooksByBookName(userSearchKeyBook);
                break;
            case "isbn":
                sellingBooks = bookFacade.findBooksByBookIsbn(userSearchKeyBook);
                break;
        }
    }
    
    public void showBookDetailDialog(int bID) {
//        this.selectedBookId = bID;
        
        System.out.println(bID);
        Map<String, Object> options = new HashMap<String, Object>();
        options.put("modal", true);
        options.put("width", 900);
        options.put("height", 520);
        options.put("contentWidth", "100%");
        options.put("contentHeight", "100%");
//        options.put("position", "absolute");
//        options.put("closable", true);

        String outcome = "/displayBooks";
        Book book = findOneBookByBookID(bID);

        Map<String, List<String>> params = new HashMap<>();

        params.put("book-name", Arrays.asList(book.getBookName()));
        params.put("book-price", Arrays.asList(Integer.toString(book.getSellPrice())));
        params.put("book-college", Arrays.asList(book.getCollege()));
        params.put("book-major", Arrays.asList(book.getMajor()));
        params.put("teacherName", Arrays.asList(book.getTeacherName()));
        params.put("book-isbn", Arrays.asList(book.getIsbn()));
        params.put("book-status", Arrays.asList(book.getStatus()));
        params.put("book-sellerID", Arrays.asList(book.getSellerID()));
        params.put("book-id", Arrays.asList(Integer.toString(book.getId().intValue())));
        System.out.println(params);
        //params.put("book-note", Arrays.asList(book.getNote()));

        PrimeFaces.current().dialog().openDynamic(outcome, options, params);
    }
    
    
    public String exitBookDetailDialog() {
        PrimeFaces.current().dialog().closeDynamic(new Object());
        return "/index" ;
    }
    
    public Book findOneBookByBookID(int bID){
        return bookFacade.findAll().stream()
                .filter(aBook -> aBook.getId() == bID)
                .findFirst().get();
    }
    
    public String renderToSearchBook(){
        return "/member/searchBook";
    }

    @PostConstruct
    public void init() {
        //findAllSellingBook();
        sellingBooks = bookFacade.findAll().stream()
                .filter(aBook -> aBook.getSituation().equals("selling"))
                .collect(Collectors.toList());
    }
}