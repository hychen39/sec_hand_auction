/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import java.io.IOException;
import java.io.Serializable;
import java.util.Base64;
import javax.enterprise.context.Dependent;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.file.UploadedFile;

/**
 * Handling the image uploading.
 * 
 * @author hychen39@gm.cyut.edu.tw
 */
@Named(value = "uploadFileBean")
@Dependent
@Getter
@Setter
public class UploadFileBean implements Serializable {
    private UploadedFile uploadedFile;
    private byte [] uploadedFileByteArray;
    /**
     * Base64 encoding for the image
     */
    private String image;
    
    /**
     * Creates a new instance of UploadFileBean
     */
    public UploadFileBean() {
    }
    
    /**
     * FileUploadEvent handler for uploading file action.
     * 
     * Populate the image content to {@link #uploadedFileByteArray} and {@link #image}
     * 
     * @param event
     */
    public void handleFileUpload(FileUploadEvent event) throws IOException {
        uploadedFile = event.getFile();
        // Read the file contents from the input stream.
        uploadedFileByteArray = uploadedFile.getContent();
        
        this.image = toBase64(uploadedFileByteArray);
        
        FacesMessage message = new FacesMessage("Succesful",
                uploadedFile.getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, message);

    }
    
    /**
     * Convert to base64 string,
     * @param image
     * @return 
     */
    private String toBase64(byte [] image){
        String base64Str = Base64.getEncoder().encodeToString(image);
        return "data:image/png;base64," + base64Str;
        
    }
    
}
