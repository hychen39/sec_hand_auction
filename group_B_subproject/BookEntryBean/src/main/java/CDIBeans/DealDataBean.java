/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import entities.Book;
import entities.DealData;
import facade.DealDataRepositoryImpl;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author yaosiyu
 */
@Named(value = "dealDataBean")
@ViewScoped
public class DealDataBean implements Serializable{
    private DealData dealData;

    
    public DealData getDealData() {
        return dealData;
    }

    public void setDealData(DealData dealData) {
        this.dealData = dealData;
    }
//    Done: 學號沒有加 s, 和給定的資料不一致. @hychen39, sp5
    private String studentID = "s10814111";
    
    private int orderID = 1;
    
    @EJB
    DealDataRepositoryImpl dealDataRepository;

    public List<DealData> findDealDatasById() {
        // use the Stream feature in Java 8
        return dealDataRepository.findAll().stream()
                .filter(aDealData -> aDealData.getBuyerID().equals(this.studentID))
                .collect(Collectors.toList());

    }
    
    public String buyerClickDeal(){
        //dealData無法加入repository
        dealData = new DealData();
        dealData.setDealDate(LocalDateTime.now());
        dealData.setOrderID(orderID++);
        dealData.setBuyerID("s10814111");
        dealData.setSellerID("s10614002");
        dealData.setOrderPrice(150);
        dealData.setDealStatus("trading");
        dealDataRepository.add(dealData);

        return "orders";
    }
    @PostConstruct
    public void init() {
    }
}
