/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import entities.Book;
import entities.Order;
import facade.BookRepositoryImpl;
import facade.OrderRepositoryImpl;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.time.temporal.ChronoUnit;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import org.primefaces.PrimeFaces;
import usermgt.UserManagementBean;
/**
 * CDI Bean for /member/orders.xhtml
 * @author yaosiyu
 */
@Named(value = "orderBean")
@ViewScoped
public class OrderBean implements Serializable{
    private Order order;
    private Book book;
    protected static int orderID;

   
    @EJB
    OrderRepositoryImpl orderRepository;
    
    @Inject
    private UserManagementBean userManagerBean;

    @EJB
    BookRepositoryImpl bookRepository;
    
//    Done: 學號沒有加 s, 和給定的資料不一致 @s10635105
    private String studentID = "s10814111";
    public String getStudentID() {
        return userManagerBean.getPrincipalName();
//        return studentID;
    }
    
    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    
       
   

    
    public List<Order> findOrdersById() {
        // use the Stream feature in Java 8
        return orderRepository.findAll().stream()
                .filter(aDealData -> aDealData.getBuyerID().equals(this.studentID))
                .collect(Collectors.toList());

    }
    
    public List<Order> getAllOrder(){
        return orderRepository.findAll().stream()
                .collect(Collectors.toList());
    }
    
    
    /**
     * 新增一筆訂單並把{@code bookSituation}設為"checking"
     * @return 
     */
    public String buyerClickDeal(){
        FacesContext fc = FacesContext.getCurrentInstance();
        int bookID = Integer.parseInt(fc.getExternalContext().getRequestParameterMap().get("book-id"));
        order = new Order();
//        int bID = Integer.parseInt(bookID);

        //不知道如何找到被點擊的book 先暫時用固定book
        book = bookRepository.query(bookID);
        order.setTxnDate(LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS));
        order.setId(++orderID);
        order.setBuyerID("s10814111");
        order.setSellerID(book.getSellerID());
        order.setAmount(book.getSellPrice());
        order.setBookID(bookID);
        order.setOrderStatus("trading");
        orderRepository.add(order);
        book.setSituation("checking");
        PrimeFaces.current().dialog().closeDynamic(new Object());
        return "orders";
    }
    
    /**
     * 賣家按下已出售, 系統找出所有的訂單 用書的id去比對每筆訂單記錄的{@code bookID} 找出相對應的訂單並把狀態改為complete.
     * @param index 
     */
    public void sellerClickSold(int bookId){
        for(Order o : getAllOrder()){
            if(o.getBookID() == bookId)
               o.setOrderStatus("complete");
        }
    }
    
    /**
     * 賣家按下重新上架, 系統找出所有訂單與相應書本比對 找到相應訂單把{@code orderStatus}設為cancel.
     * @param index 
     */
    public void sellerClickRelist(int bookId){
        for(Order o : getAllOrder()){
            if(o.getBookID() == bookId)
                o.setOrderStatus("cancel");
        }
    }
    
    @PostConstruct
    public void init() {
    }
}
