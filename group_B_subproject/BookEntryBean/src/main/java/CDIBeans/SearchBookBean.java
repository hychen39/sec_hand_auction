/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import entities.SearchBook;
import facade.SearchBookRepositoryImpl;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.view.ViewScoped;
import lombok.Getter;
import lombok.Setter;


/**
 *
 * @author yaosiyu
 */

@Getter
@Setter

@Named(value = "searchBookBean")
@ViewScoped
public class SearchBookBean implements Serializable {

    private SearchBook searchBook;
    private int searchID;
    /**
     * Creates a new instance of SearchBookBean
     */
    public SearchBookBean() {
    }
    
    @EJB
    SearchBookRepositoryImpl searchBookRepository;
    
    /**
     * 設定尋書單的編號和時間, 並添加一筆尋書單到儲存庫
     */
    public void addSearchBook(){
        searchBook.setId(++searchID);
        searchBook.setSearchTime(LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS));
        searchBook.setSeeker("s10814111");
        searchBookRepository.add(searchBook);
        searchBook = new SearchBook();
    }
    
    public List<SearchBook> findSearchingBookById(){
        return searchBookRepository.findAll().stream()
                .filter(aSearchBook -> aSearchBook.getSeeker().equals("s10814111"))
                .collect(Collectors.toList());
    }
    
    public void deleteSearchingBook(SearchBook s){
        searchBookRepository.delete(s.getId());
    }
    
    @PostConstruct
    public void init(){
        this.searchBook = new SearchBook();
    }
    

}
