/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usermgt;

import java.security.Principal;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 * EJB for User management functions
 *
 * @author hychen39@gm.cyut.edu.tw
 */
@Stateless
public class UserManagementBean {

    @Inject
    private Principal principal;

    @Inject
    private HttpServletRequest httpRequest;

    /**
     * If not logged in this returns "ANONYMOUS" on GF
     * Fixme: Hard code user id. hychen39@sp6
     * @return
     */
    public String getPrincipalName() {
        return "s10814111";
//        return principal.getName();
    }
    
    /**
     * Fixme: Hard code the statue hychen39@sp6
     * @return 
     */
    public boolean isLoginStatus() {
//        String remoteUser  = httpRequest.getRemoteUser();
//        return (remoteUser != null);
    return true;
    }
    
    public String logout() {
        try {
            httpRequest.logout();
        } catch (ServletException ex) {
            Logger.getLogger(UserManagementBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }
    
}
