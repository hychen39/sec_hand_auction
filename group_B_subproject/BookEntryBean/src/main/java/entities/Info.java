/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * The User Info used in the {@link User} entity.
 * @author hychen39@gm.cyut.edu.tw
 * @since Oct 4, 2020
 */
@Embeddable
@Getter
@Setter
@EqualsAndHashCode
public class Info {

    // TODO: 用途？
    private int infoNum;
    // TODO: 用途？
    private boolean looked;
    // TODO: 用途？
    private String redirect;

}
