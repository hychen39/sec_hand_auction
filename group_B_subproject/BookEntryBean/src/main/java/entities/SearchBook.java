/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import lombok.Getter;
import lombok.Setter;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;

/**
 *
 * @author yaosiyu
 */
@Entity
@Getter
@Setter
@EqualsAndHashCode
@Table(name = "SEARCH_BOOK")
public class SearchBook {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "SEARCH_TIME", columnDefinition = "DATE")
    private LocalDateTime searchTime;
    private String seeker;
    private String courseName;
    private String teacher;
}
