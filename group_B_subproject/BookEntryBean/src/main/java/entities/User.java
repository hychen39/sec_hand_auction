/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author yaosiyu
 */
@Entity
@Getter
@Setter
@EqualsAndHashCode
@Table(name = "APP_USER")
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String mail;
    private String password;
    private String passwordConfirm;
    // ManyToMany Relationship
    @ManyToMany(cascade = CascadeType.ALL)
    // Specify the join column names in the join table
    @JoinTable(name = "USER_FAVORITE_BOOK" , 
            joinColumns = @JoinColumn(name="USER_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "BOOK_ID", referencedColumnName = "ID"))
    private List<Book> favorite; //儲存使用者收藏的書籍
    @Embedded
    private Info info;

    public User() {
    }

    public User(User user) {
        this.mail = user.mail;
        this.password = user.password;
        this.passwordConfirm = user.passwordConfirm;
    }

    public User(String mail, String password, String passwordConfirm) {
        this.mail = mail;
        this.password = password;
        this.passwordConfirm = passwordConfirm;
    }

//    public String getMail() {
//        return mail;
//    }
//
//    public void setMail(String mail) {
//        this.mail = mail;
//    }
//
//    public String getPassword() {
//        return password;
//    }
//
//    public void setPassword(String password) {
//        this.password = password;
//    }
//
//    public String getPasswordConfirm() {
//        return passwordConfirm;
//    }
//
//    public void setPasswordConfirm(String passwordConfirm) {
//        this.passwordConfirm = passwordConfirm;
//    }
}
