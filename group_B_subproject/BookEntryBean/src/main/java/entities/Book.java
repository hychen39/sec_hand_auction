/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * ToDo: <a href="https://hychen39.atlassian.net/browse/UBA-34">
 *       UBA-34 </a> hychen39@SP7
 * @author yaosiyu
 */
@Entity
@Getter
@Setter
@EqualsAndHashCode
@NamedQueries({
    @NamedQuery(name="findBookBySellerID", 
                query="select b from Book b where b.sellerID = :sellerID"),
    @NamedQuery(name="findBookByBookName", 
                query="select b from Book b where b.bookName = :bookName"),
    @NamedQuery(name="findBookByBookIsbn", 
                query="select b from Book b where b.isbn = :isbn")
})
public class Book {
    
    //Primary key
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    private String isbn;
    private int sellPrice;
    /**
     * {@code status}為書況等級 分為A,B,C
     */
    private String status;
    private String bookName;
    private String college;
    private String major;
    private String teacherName;
    private String sellerID;
    @Column(name = "CREATE_TIME", columnDefinition = "TIMESTAMP")
    private Timestamp createdTime;
    /**
     * {@code situation}為書本銷售狀況 分為selling, checking, complete
     *      賣家刊登後 還沒有買家購買 {@code situation}設為selling
     *      買家按下購買 賣家按下重新上架 {@code situation}設為selling
     *      買家按下購買 賣家未按下按鈕 {@code situation}設為checking
     *      買家按下購買 賣家按下已出售 {@code situation}設為complete
     */
    private String situation;
    
    private String note;
    /**
     * 主要刊登圖片
     */
    @Lob
    private byte [] featureImg;
    
    // Constructors
    public Book() {
    }

    public Book(String isbn, int sellPrice, String status, String bookName, String college, String major, String teacherName, String sellerID, String situation) {
        this.isbn = isbn;
        this.sellPrice = sellPrice;
        this.status = status;
        this.bookName = bookName;
        this.college = college;
        this.major = major;
        this.teacherName = teacherName;
        this.sellerID = sellerID;
        this.situation = situation;
    }
}
