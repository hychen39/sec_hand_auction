/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import data_access.BookRepository;
import entities.Book;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 *
 * @author yaosiyu
 */
@Named(value = "bookBean")
@SessionScoped
public class BookBean implements Serializable {
    private Book book;
    private String studentID = "10635105";
    private List<Book> sellingResult;

    public List<Book> getSellingResult() {
        return sellingResult;
    }

    public void setSellingResult(List<Book> sellingResult) {
        this.sellingResult = sellingResult;
    }
    
    @Inject
    private BookRepository bookrepository;
    
    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }
    
    /**
     * Creates a new instance of BookBean
     */
    public BookBean() {
    }
    
    public String sendToRepository(){
        bookrepository.putToSelling(studentID,book);
        this.sellingResult = bookrepository.retrieveFrom(studentID, "Selling");
        return "member";
    }
    //bean被建立時執行的事
    @PostConstruct
    public void init(){
        this.book = new Book();
        this.sellingResult = new ArrayList();
        this.sellingResult = bookrepository.retrieveFrom(studentID, "Selling");
        //bookrepository.putToSelling("10635105", new Book("134", 1, "a", "b", "c", "d", "e"));
    }
    
}
