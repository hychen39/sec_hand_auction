/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import entities.Buyer;
import entities.User;
import facade.UserRepositoryImpl;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.annotation.PostConstruct;

/**
 *
 * @author yaosiyu
 */
@Named(value = "userBean")
@SessionScoped
public class UserBean implements Serializable{

    private User user;
    private User rightUser;
    private int recentNum = 0;
    private int memberNum;
    private boolean joinStatus = false;
    private boolean loginStatus = false;
    int studentID = 0;
    private Buyer buyer;
    
    UserRepositoryImpl userRepository = new UserRepositoryImpl();
    
    @PostConstruct
    public void init(){
        this.user = new User();
    }

    public Buyer getBuyer() {
        return buyer;
    }

    public void setBuyer(Buyer buyer) {
        this.buyer = buyer;
    }

    
    public int getRecentNum() {
        return recentNum;
    }

    public void setRecentNum(int recentNum) {
        this.recentNum = recentNum;
    }

    public int getMemberNum() {
        return memberNum;
    }

    public void setMemberNum(int memberNum) {
        this.memberNum = memberNum;
    }

    public boolean isJoinStatus() {
        return joinStatus;
    }

    public void setJoinStatus(boolean joinStatus) {
        this.joinStatus = joinStatus;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    

    /**
     * Creates a new instance of userBean
     */
    public UserBean() {
    }

    public String execJoin() {
        memberNum = userRepository.add(user);
        if(memberNum == ++this.recentNum){
            joinStatus = true;
            return "login";
        }
        else{
            joinStatus = false;
            return "join";
        }
    }
    
    public String execLogin(){
        studentID = Integer.parseInt(user.getMail().substring(1, 9));
        rightUser = userRepository.query(studentID);
        
        
        if(rightUser == null || !rightUser.getPassword().equals(this.user.getPassword())){
            loginStatus = false;
            return "login";
        }
        else{
            loginStatus = true;
            return "home";
        }
    }
}
