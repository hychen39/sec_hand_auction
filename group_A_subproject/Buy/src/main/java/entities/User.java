/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author yaosiyu
 */
public class User {
    private String mail;
    private String password;
    private String passwordConfirm;
    
//    public User(User user){
//        this.mail = user.mail;
//        this.password = user.password;
//        this.passwordConfirm = user.passwordConfirm;
//    }
    
//    public User(String mail,String password,String passwordConfirm){
//        this.mail = mail;
//        this.password = password;
//        this.passwordConfirm = passwordConfirm;
//    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }
    
}
