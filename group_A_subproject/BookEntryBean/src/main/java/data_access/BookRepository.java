/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data_access;
import entities.Book;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;

/**
 * @deprecated  since 2020/5/12
 * @author yaosiyu
 */

@Singleton
public class BookRepository {
    private Map<String,Map<String,ArrayList>> repository;

    @PostConstruct
    public void init(){
        repository = new Hashtable<>();
        
        Hashtable<String, ArrayList> tmp = new Hashtable<>();
        tmp.put("Selling", new ArrayList());
        tmp.put("Checking", new ArrayList());
        tmp.put("Sold", new ArrayList());
        tmp.put("Bought", new ArrayList());
        repository.put("10635105", tmp);
    }
    
    public void putToSelling(String ID,Book book){
        repository.get(ID).get("Selling").add(book);
    }
    
    public ArrayList retrieveFrom(String ID,String type){
        return repository.get(ID).get(type);
    }
}
