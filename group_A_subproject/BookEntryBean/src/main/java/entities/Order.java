/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.time.LocalDateTime;

/**
 * ToDo: <a href="https://hychen39.atlassian.net/browse/UBA-34">
 *       UBA-34 </a> hychen39@SP7
 * Demand Req: U10
 * @author yaosiyu
 */
public class Order {
    private int id;
    private LocalDateTime txnDate;
    private int amount;
    private String buyerID;
    private String sellerID;
    private int bookID;
    /**
     * {@code orderStatus}為訂單狀態 分為complete,cancel,trading
     *      買家按下購買 賣家未按下任何按鈕 {@code orderstatus}設為trading
     *      買家按下購買 賣家按下已出售 {@code orderStatus}設為complete
     *      買家按下購買 賣家按下重新上架 {@code orderStatus}設為cancel
     */
    private String orderStatus;

    public int getBookID() {
        return bookID;
    }

    public void setBookID(int bookID) {
        this.bookID = bookID;
    }
    
    public Order(){
        
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(LocalDateTime txnDate) {
        this.txnDate = txnDate;
    }


    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getBuyerID() {
        return buyerID;
    }

    public void setBuyerID(String buyerID) {
        this.buyerID = buyerID;
    }

    public String getSellerID() {
        return sellerID;
    }

    public void setSellerID(String sellerID) {
        this.sellerID = sellerID;
    }
}
