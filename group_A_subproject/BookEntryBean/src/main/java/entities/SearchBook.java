/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import lombok.Getter;
import lombok.Setter;
import java.time.LocalDateTime;

/**
 *
 * @author yaosiyu
 */
@Getter
@Setter
public class SearchBook {
    private int id;
    private LocalDateTime searchTime;
    private String seeker;
    private String courseName;
    private String teacher;
}
