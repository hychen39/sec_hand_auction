/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import lombok.Getter;
import lombok.Setter;

/**
 * ToDo: <a href="https://hychen39.atlassian.net/browse/UBA-34">
 *       UBA-34 </a> hychen39@SP7
 * @author yaosiyu
 */
@Getter
@Setter
public class Book {

    private String isbn;
    private int sellPrice;
    /**
     * {@code status}為書況等級 分為A,B,C
     */
    private String status;
    private String bookName;
    private String college;
    private String major;
    private String teacherName;
    private String sellerID;
    /**
     * {@code situation}為書本銷售狀況 分為selling, checking, complete
     *      賣家刊登後 還沒有買家購買 {@code situation}設為selling
     *      買家按下購買 賣家按下重新上架 {@code situation}設為selling
     *      買家按下購買 賣家未按下按鈕 {@code situation}設為checking
     *      買家按下購買 賣家按下已出售 {@code situation}設為complete
     */
    private String situation;
    private int id;
    private String note;
    /**
     * 主要刊登圖片
     */
    private byte [] featureImg;
    
    // Constructors
    public Book() {
    }

    public Book(String isbn, int sellPrice, String status, String bookName, String college, String major, String teacherName, String sellerID, String situation) {
        this.isbn = isbn;
        this.sellPrice = sellPrice;
        this.status = status;
        this.bookName = bookName;
        this.college = college;
        this.major = major;
        this.teacherName = teacherName;
        this.sellerID = sellerID;
        this.situation = situation;
    }

//    public String getNote() {
//        return note;
//    }
//
//    public void setNote(String note) {
//        this.note = note;
//    }
//
//    public int getId() {
//        return id;
//    }
//    
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    
//    public String getSituation() {
//        return situation;
//    }
//
//    public void setSituation(String situation) {
//        this.situation = situation;
//    }
//
//    public String getIsbn() {
//        return isbn;
//    }
//
//    public void setIsbn(String isbn) {
//        this.isbn = isbn;
//    }
//
//    public int getSellPrice() {
//        return sellPrice;
//    }
//
//    public void setSellPrice(int sellPrice) {
//        this.sellPrice = sellPrice;
//    }
//    
//    public String getStatus() {
//        return status;
//    }
//
//    public void setStatus(String status) {
//        this.status = status;
//    }
//
//    public String getSellerID() {
//        return sellerID;
//    }
//
//    public void setSellerID(String sellerID) {
//        this.sellerID = sellerID;
//    }
//    
//    public String getBookName() {
//        return bookName;
//    }
//
//    public void setBookName(String bookName) {
//        this.bookName = bookName;
//    }
//    
//    public String getCollege() {
//        return college;
//    }
//
//    public void setCollege(String college) {
//        this.college = college;
//    }
//
//    public String getMajor() {
//        return major;
//    }
//
//    public void setMajor(String major) {
//        this.major = major;
//    }
//
//    public String getTeacherName() {
//        return teacherName;
//    }
//
//    public void setTeacherName(String teacherName) {
//        this.teacherName = teacherName;
//    }
}
