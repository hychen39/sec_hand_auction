/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 * Used Page: /member/addbook.xhtml. 
 * Done: 修正變數命名. program ==> college; class
 * ==> department. hychen39@sp6 提供學院及學系的下拉式清單. 
 * 
 * ToDo: UBA-25. See https://hychen39.atlassian.net/browse/UBA-25 hychen39@SP7
 *
 * @author hychen39@gm.cyut.edu.tw
 */
@Named(value = "addBookBean")
@ViewScoped
public class AddBookBean implements Serializable {
    final static public String MANAGEMENT_COLLEGE = "mcollege";
   /** final static public String MANAGEMENT_COLLEGE = "管理學院";*/
    final static public String ENGINEERING_COLLEGE = "soe";
    final static public String DESIGN_COLLEGE = "dcollege";
    final static public String HUMANITIES_COLLEGE = "humanity";
    final static public String INFORMATION_COLLEGE = "coinfo";
    final static public String GE_COLLEGE = "ge";
    private String college;
    private Map<String, List<SelectItem>> departments;
    /**
     * Creates a new instance of AddBookBean
     */
    public AddBookBean() {
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public Map<String, List<SelectItem>> getDepartments() {
        return departments;
    }

    public void setDepartments(Map<String, List<SelectItem>> departments) {
        this.departments = departments;
    }

    @PostConstruct
    public void init() {
        this.college = AddBookBean.MANAGEMENT_COLLEGE;
        departments = new HashMap<>();
        departments.put(AddBookBean.MANAGEMENT_COLLEGE,
                Arrays.asList(mcollegeDepartmentList()));
        departments.put(AddBookBean.ENGINEERING_COLLEGE,
                Arrays.asList(soeDepartmentList()));
        departments.put(AddBookBean.DESIGN_COLLEGE,
                Arrays.asList(dcollegeDepartmentList()));
        departments.put(AddBookBean.HUMANITIES_COLLEGE,
                Arrays.asList(humanityDepartmentList()));
        departments.put(AddBookBean.INFORMATION_COLLEGE,
                Arrays.asList(coinfoDepartmentList()));
        departments.put(AddBookBean.GE_COLLEGE,
                Arrays.asList(geDepartmentList()));
    }

    /**
     * 理工學院學系清單
     * @return 
     */
    public SelectItem[] soeDepartmentList() {
        SelectItem soe_departments[] = new SelectItem[]{
            new SelectItem("ce", "營建工程系"),
            new SelectItem("iem", "工業工程與管理系"),
            new SelectItem("applchem", "應用化學系"),
            new SelectItem("dem", "環境工程與管理系"),
            new SelectItem("aerome", "航空機械系"),};
        return soe_departments;
    }

    /**
     * 管理學院學系清單
     *
     * @return
     */
    public SelectItem[] mcollegeDepartmentList() {
        SelectItem mcollege_departments[] = new SelectItem[]{
            new SelectItem("finance", "財務金融系"), /** new SelectItem("財務金融系")*/
            new SelectItem("ba", "企業管理系"),
            new SelectItem("ins", "保險金融管理系"),
            new SelectItem("acc", "會計系"),
            new SelectItem("leisure", "休閒事業管理系"),
            new SelectItem("marketing", "行銷與流通管理系"),
            new SelectItem("gaim", "銀髮產業管理系"),};
        return mcollege_departments;
    }

    /**
     * 設計學院學系清單
     *
     * @return
     */
    public SelectItem[] dcollegeDepartmentList() {
        SelectItem dcollege_departments[] = new SelectItem[]{
            new SelectItem("arch", "建築系"),
            new SelectItem("id", "工業設計系"),
            new SelectItem("visual", "視覺傳達設計系"),
            new SelectItem("upla", "景觀及都市設計系"),};
        return dcollege_departments;
    }
   /**
     * 人文社會學院學系清單
     *
     * @return
     */
    public SelectItem[] humanityDepartmentList() {
        SelectItem humanity_departments[] = new SelectItem[]{
            new SelectItem("ft", "傳播藝術系"),
            new SelectItem("aecyut", "應用英語系"),
            new SelectItem("ecde", "幼兒保育系"),
            new SelectItem("swdept", "社會工作系"),};
        return humanity_departments;
    }
     /**
     * 資訊學院學系清單
     *
     * @return
     */
     public SelectItem[] coinfoDepartmentList() {
        SelectItem coinfo_departments[] = new SelectItem[]{
            new SelectItem("im", "資訊管理系"),
            new SelectItem("csie", "資訊工程系"),
            new SelectItem("ice", "資訊與通訊系"),};
        return coinfo_departments;
    }
      /**
     * 通識教育清單
     *
     * @return
     */
     public SelectItem[] geDepartmentList() {
        SelectItem ge_departments[] = new SelectItem[]{
            new SelectItem("ge", "通識"),};
        return ge_departments;
    }
    /**
     * 傳回特定學院(college)下的學系(department)名稱
     *
     * @param college
     * @return
     */
    public List<SelectItem> findDepartments(String college) {
        if (college == null || "".equals(college)) {
            college = this.college;
        }
//        System.out.println("Program: " + program);
        return this.departments.get(college);
    }

}
