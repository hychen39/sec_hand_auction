/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;

/**
 * /member/personalData.xhtml, /dialogs/changPassword.xhtml 使用的 CDI bean.
 * @author yaosiyu
 */
@Named(value = "personalDataBean")
@RequestScoped
public class PersonalDataBean {

    FacesContext facesContext;

    @PostConstruct
    public void init() {
        facesContext = FacesContext.getCurrentInstance();
    }

    /**
     * Creates a new instance of personalDataBean
     */
    public PersonalDataBean() {
    }

    public String showAddBookDialog() {
        Map<String, Object> options = new HashMap<String, Object>();
        options.put("modal", true);
        options.put("width", 640);
        options.put("height", 340);
        options.put("contentWidth", "100%");
        options.put("contentHeight", "100%");
        options.put("headerElement", "customheader");

        String outcome = "/member/addBook";

        Map<String, List<String>> params = new HashMap<>();

        params.put("dialog-title", Arrays.asList("請填寫書本詳細資料"));

        PrimeFaces.current().dialog().openDynamic(outcome, options, params);

        return null;
    }

    public String showChangePasswordDialog() {
        Map<String, Object> options = new HashMap<String, Object>();
        options.put("modal", true);
        options.put("width", 640);
        options.put("height", 400);
        options.put("contentWidth", "100%");
        options.put("contentHeight", "100%");
        options.put("headerElement", "customheader");

        String outcome = "/dialogs/changePassword";

        Map<String, List<String>> params = new HashMap<>();

        params.put("dialog-title", Arrays.asList("Add book form"));
        params.put("dialog-message", Arrays.asList("Which one do you prefer?"));

        PrimeFaces.current().dialog().openDynamic(outcome, options, params);

        return null;
    }

    public String exitChangePasswordDialog() {
        PrimeFaces.current().dialog().closeDynamic(new Object());
        return "/member/personalData" ;
    }
}
