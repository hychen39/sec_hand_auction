/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import entities.Book;
import facade.BookRepositoryImpl;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.primefaces.PrimeFaces;
import java.util.Map;
import java.util.function.Predicate;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import org.primefaces.model.file.UploadedFile;
import usermgt.UserManagementBean;

/**
 *
 * @author yaosiyu
 */
@Named(value = "bookBean")
//Done: 考慮改成 ViewScoped. 不然就是在提交資料後, 要重置 book property. 如此, 先前的資料才不會一直留著.  @hychen39, sp4. Done sp5
@ViewScoped
public class BookBean implements Serializable {

    private Logger logger = Logger.getLogger(BookBean.class.getName());
    private Book book;
    private String studentID = "";
    
    @Inject
    private UploadFileBean uploadFileBean;

    // Inject 單體 EJB BookRepositoryImpl 的實體. 不用自己 new 實體
    @EJB
    BookRepositoryImpl bookRepository;
    
    @Inject
    private LoginBean loginBean;

    public UploadFileBean getUploadFileBean() {
        return uploadFileBean;
    }

    
    /**
     * Done: delete the property. Use injected EJB instance. 已刪除s10635105
     *
     * @deprecated property since 2020/5/12
     */
    public List<Book> findAllBooks() {
        return new ArrayList<Book>(bookRepository.findAll());
    }

    public List<Book> findBooksById() {
        // use the Stream feature in Java 8
        return bookRepository.findAll().stream()
                .filter(aBook -> aBook.getSellerID().equals(this.studentID))
                .collect(Collectors.toList());
    }


    public List<Book> getAllPersonalBook() {
//        return allPersonalBook;
        studentID = loginBean.getUsername();        
        logger.info(String.format("Find book for the user: %s", studentID));
        return bookRepository.findPersonalBook(studentID);
    }


    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    /**
     * Creates a new instance of BookBean
     */
    public BookBean() {
    }

    /**
     * 新增要銷售的書本。 書本的 {@code sellerID} 被預設為登入者的 id. 書本的 {@code situation} 被預設
     * "selling"
     *
     * @return
     */
    public String addSellingBook() {
        book.setSituation("selling");
        book.setSellerID(this.studentID);
//        Done: 欄位名稱應用小寫開頭駝峰字, 改成: bookRepository  @hychen39, sp4
        book.setId(bookRepository.add(book));
        book.setFeatureImg(uploadFileBean.getUploadedFileByteArray());
        System.out.println("Book title: " + book.getBookName() + "Repository size: " + bookRepository.size() + "bookID: " + book.getId());
//        bookrepository.add(book);
//        this.allPersonalBook = bookrepository.findPersonalBook(studentID);
//        findSellingBookToMP();
//clean the book
        this.book = new Book();
        PrimeFaces.current().dialog().closeDynamic(new Object());
        return "/member/member";
    }

    /**
     * 找出買家目前正在銷售的書本. 找到的資料存暫時加入 {@code #sellingBook} 欄位,結束後回傳. MP為 "我的商品"頁面的簡稱
     *
     */
//    Done: 說明 method 的功能, 無法從名稱中看出來. 結尾的 MP 代表什麼意思? @hychen39, sp4 已加入註解s10635105
//    Done: method 的註解說明方式. @hychen39, sp5
    public List<Book> findSellingBookToMP() {
        List<Book> sellingBook = new ArrayList();
        for (Book b : getAllPersonalBook()) {
            if (b.getSituation().equals("selling")) {
                sellingBook.add(b);
            }
        }
        return sellingBook;
    }
    
    /**
     * 找出賣家目前被購買的書(未確認該書已出售還是要再次上架) 找到的資料暫時加入{@code checkingBook}欄位,結束後回傳. MP為 "我的商品"頁面的簡稱
     * @return 
     */
    public List<Book> findCheckingBookToMP(){
        List<Book> checkingBook = new ArrayList();
        for(Book b : getAllPersonalBook()){
            if(b.getSituation().equals("checking"))
                checkingBook.add(b);
        }
        return checkingBook;
    }
    
    /**
     * 賣家按下已出售, 系統找出哪本書已出售把這本書的{@code situation}改為complete.
     * @param index 
     */
    public void sellerClickSold(int bookId){
        book = bookRepository.query(bookId);
        book.setSituation("complete");
    }
    
    /**
     * 賣家按下重新上架, 系統找出此書並把{@code situation}設為selling.
     * @param index 
     */
    public void sellerClickRelist(int bookId){
        book = bookRepository.query(bookId);
        book.setSituation("selling");
    }

    //bean被建立時執行的事
    @PostConstruct
    public void init() {
        this.book = new Book();
//        Done: 學號編碼要統一, 有的加 s, 有的沒有加 s, 不一致.  @hychen39, sp4 修改為統一加s的格式s10635105
//        studentID = "s10614002";
        studentID = "s10814111";
    }

}
