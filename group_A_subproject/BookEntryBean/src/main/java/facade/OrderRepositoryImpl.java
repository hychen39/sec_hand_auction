/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import entities.Order;
import java.time.LocalDateTime;
import java.util.HashMap;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import repository.AbstractRepository;

/**
 *
 * @author yaosiyu
 */
@Singleton
public class OrderRepositoryImpl extends AbstractRepository<Order>{
    private int orderID;
    public OrderRepositoryImpl(){
        super(new HashMap<Integer,Order>());
    }
    
    @PostConstruct
    public void init(){
    }
}
