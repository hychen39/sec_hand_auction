/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import entities.DealData;
import java.time.LocalDateTime;
import java.util.HashMap;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import repository.AbstractRepository;

/**
 *
 * @author yaosiyu
 */
@Singleton
public class DealDataRepositoryImpl extends AbstractRepository<DealData>{
    private int orderID;
    public DealDataRepositoryImpl(){
        super(new HashMap<Integer,DealData>());
    }
    
    @PostConstruct
    public void init(){
        creat_mockup_data();
    }
    
    public void creat_mockup_data(){
        DealData dealData = new DealData();
        dealData.setDealDate(LocalDateTime.now());
        dealData.setOrderID(orderID++);
        dealData.setBuyerID("s10814111");
        dealData.setSellerID("s10614002");
        dealData.setOrderPrice(150);
        dealData.setDealStatus("trading");
        
        this.add(dealData);
    }
}
