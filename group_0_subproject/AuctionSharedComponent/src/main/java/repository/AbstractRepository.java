/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package repository;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Repository 抽像類別
 * @author hychen39@gm.cyut.edu.tw
 * @since Mar 17, 2020
 */
public abstract class AbstractRepository<T> {
    private Map<Integer, T> repository;
    private Integer lastIndex;

    public AbstractRepository(Map<Integer, T> repository) {
        this.repository = repository;
        lastIndex = 1;
    }
    
    /**
     * Add an entity to the repository.
     * 儲存庫計數器會自動加 1. 傳回 entity 的索引編號.
     * @param entity
     * @return index of the entity
     */
    public int add(T entity){
        this.repository.put(lastIndex, entity);
        int currentIdx = lastIndex.intValue();
        lastIndex++;
        return currentIdx;
    }
    
    /**
     * 使用 <code>newEntity</code> 更新現有的 entity 的內容.
     * @param id index for the existing entity
     * @param newEntity entity containing new contents
     * @return false if the updating fails
     */
    public boolean update(Integer id, T newEntity){
        T entity = this.repository.replace(id, newEntity);
        return (entity!=null);
    }
    
    public boolean delete(Integer id){
        T entity = this.repository.remove(id);
        return (entity!=null);
    }
    
    public T query(Integer id){
        return this.repository.get(id);
    }
    
    /**
     * Find all values in the repository
     * @return 
     */
    public Collection<T> findAll(){
        return this.repository.values();
    }
    
    
    
    
    
    
    
}
