
Steps to use the project:

1. Install the project jar to the local maven repository by running the following command:
```
mvn install:install-file
```

You should see the following results:
```
$ mvn install:install-file
[INFO] Scanning for projects...
[INFO]
[INFO] -----------------< me.hychen39:AuctionSharedComponent >-----------------
[INFO] Building AuctionSharedComponent 1.0
[INFO] --------------------------------[ jar ]---------------------------------
[INFO]
[INFO] --- maven-install-plugin:2.5.1:install-file (default-cli) @ AuctionSharedComponent ---
[INFO] Installing D:\108_auction_project\group_0_subproject\AuctionSharedComponent\target\AuctionSharedComponent-1.0.jar to C:\Users\user\.m2\repository\me\hychen39\AuctionSharedComponent\1.0\AuctionSharedComponent-1.0.jar
[INFO] Installing C:\Users\user\AppData\Local\Temp\mvninstall4475295724559027288.pom to C:\Users\user\.m2\repository\me\hychen39\AuctionSharedComponent\1.0\AuctionSharedComponent-1.0.pom
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  1.189 s
[INFO] Finished at: 2020-03-22T20:06:06+08:00
[INFO] ------------------------------------------------------------------------
```

2. Go the your client project. Add the following dependency to its `pom.xml`
```
<dependency>
    <groupId>me.hychen39</groupId>
    <artifactId>AuctionSharedComponent</artifactId>
    <version>1.0</version>
</dependency>
```