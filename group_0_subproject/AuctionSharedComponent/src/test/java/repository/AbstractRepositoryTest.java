/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import entities.Book;
import java.util.HashMap;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Assert;

/**
 *
 * @author hychen39@gm.cyut.edu.tw
 */
public class AbstractRepositoryTest{
    private AbstractRepositoryImpl respo;
    private int entityId;
    
    public AbstractRepositoryTest() {
        this.respo = new AbstractRepositoryImpl();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of add method, of class AbstractRepository.
     */
    @Test
    public void testAdd() {
        Book book = new Book();
        book.setBookName("book1");
        entityId = this.respo.add(book);
        
        Assert.assertTrue(entityId >=1 );
        
    }

    /**
     * Test of update method, of class AbstractRepository.
     */
    @Test
    public void testUpdate() {
    }

    /**
     * Test of delete method, of class AbstractRepository.
     */
    @Test
    public void testDelete() {
        Book book = new Book();
        book.setBookName("book1");
        int id = this.respo.add(book);
        boolean result = this.respo.delete(id);
        Assert.assertTrue(result);
    }

    /**
     * Test of query method, of class AbstractRepository.
     */
    @Test
    public void testQuery() {
    }

    /**
     * Test of findAll method, of class AbstractRepository.
     */
    @Test
    public void testFindAll() {
        Book book1 = new Book();
        book1.setBookName("Book1");
        Book book2 = new Book();
        book2.setBookName("Book2");
        this.respo.add(book1);
        this.respo.add(book2);
        int size = this.respo.findAll().size();
        Assert.assertEquals(2, size);
    }
    //inner class
    public class AbstractRepositoryImpl extends AbstractRepository<Book>{

        public AbstractRepositoryImpl() {
            super(new HashMap<>());
        }
    }
    
}
