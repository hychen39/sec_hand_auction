# README #

108 大學部專題 108/7 ~ 109/12

二手書拍賣系統

# 組員
TBD

# 目錄

`/group_A_project` 小組 A 的專案
`/group_B_project` 小組 B 的專案

# 開發環境

JDK 11 (Default)  C:\Program Files\NetBeans-11.1\netbeans\etc\netbeans.conf 的 netbeans_jdkhome 特性

Apache Netbeans 11.x
Payara 5.x

# Releases

5/13 SP4 結束. 合併兩組的成果. 釋出 SP4 版本, 做為 SP5 的開發 baseline.
